import java.lang.UnsupportedOperationException

class NoAliveStrategy: AbstractAliveStrategy() {
	override fun check(): Boolean {
		throw UnsupportedOperationException("Alive Strategy Is Not Configured") 
	}
}
class IsNotAliveStrategy : AbstractAliveStrategy() {

	override fun check(): Boolean {
		return false
	}
}
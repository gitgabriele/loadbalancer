class TestAliveStrategy : AbstractAliveStrategy() {
	
	var count = 0;
	
	override fun check(): Boolean {
		count =	count + 1;
		return true;
	}
	
	fun callCount(): Int {
		return count;
	}
}
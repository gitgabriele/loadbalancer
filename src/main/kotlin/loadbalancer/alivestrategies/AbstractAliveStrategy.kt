abstract class AbstractAliveStrategy {
	
	abstract fun check(): Boolean
}
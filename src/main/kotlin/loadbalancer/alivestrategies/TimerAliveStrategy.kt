import java.util.Calendar

class TimerAliveStrategy : AbstractAliveStrategy() {
	
	var firstCall = 0L;
	var secondCall = 0L;
	
	override fun check(): Boolean {
		if(!firstCall.equals(0L) && !secondCall.equals(0L)) return true;
		if(firstCall.equals(0L)) {
			firstCall = System.currentTimeMillis()
		} else {
			secondCall = System.currentTimeMillis()
		}
		return true;
	}
	
	fun interval(): Long {
		return secondCall - firstCall
	}
}
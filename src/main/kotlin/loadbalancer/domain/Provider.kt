import java.util.UUID

class Provider(_uidGenerator: AbstractUIdGenerator, _aliveStrategy: AbstractAliveStrategy) {
	val uid = _uidGenerator.generate()
	var aliveStrategy = _aliveStrategy
	var alive = true;
	var counter = 0;

	constructor() : this(RandomUIdGenerator(), NoAliveStrategy()) {}
	constructor(_uidGenerator: AbstractUIdGenerator) : this(_uidGenerator, NoAliveStrategy()) {}
	constructor(aliveStrategy: AbstractAliveStrategy) : this(RandomUIdGenerator(), aliveStrategy) {}

	fun get(): String {
		return uid;
	}

	fun check() {
		if(!aliveStrategy.check()){
			exclude()
			counter = 1;
		} else {
			if(counter > 0) {
				counter = counter - 1;	
			}else if(!isAlive()) {
				include()
			}
		}
	}

	fun isAlive() : Boolean{
		return alive;
	}
	
	fun exclude() {
		this.alive = false;
	}

	fun include() {
		this.alive = true;
	}
	
	fun aliveStrategy(_aliveStrategy: AbstractAliveStrategy) {
		this.aliveStrategy = _aliveStrategy
	}
}
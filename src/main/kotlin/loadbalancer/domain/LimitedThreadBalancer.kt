import java.lang.UnsupportedOperationException
import java.util.concurrent.Executors
import java.util.concurrent.Future

class LimitedThreadBalancer(_balancer: LoadBalancer, _maxNrProvider: Int) {

	var balancer: LoadBalancer = _balancer
	val maxNrProvider = _maxNrProvider

	fun get(): String? {
		if (balancer.runningRequest() >= balancer.aliveProviders() * maxNrProvider) {
			throw UnsupportedOperationException("Too many parallel requests")
		}
		return balancer.get()
	}
}
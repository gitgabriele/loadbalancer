import java.util.concurrent.Executors
import java.util.concurrent.ScheduledFuture
import java.util.concurrent.TimeUnit

class Checker(_balancer : LoadBalancer, _heartBeatInterval: Long) {
	
	val balancer = _balancer
	val executorService = Executors.newSingleThreadScheduledExecutor()
	var heartBeatInterval = _heartBeatInterval
	var future : ScheduledFuture<*>? = null
	
	fun run() {
		future =	executorService.scheduleAtFixedRate({balancer.check()}, 0, heartBeatInterval, TimeUnit.SECONDS)
	}
	
	fun stop(){
		future?.cancel(true)
	}
}
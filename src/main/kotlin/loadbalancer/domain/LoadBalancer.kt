import java.util.UUID
import java.util.HashMap
import java.util.concurrent.atomic.AtomicInteger

class LoadBalancer(_uIdGenerator: AbstractUIdGenerator, _maxProviderNr: Int) {

	var requests: AtomicInteger = AtomicInteger(0)
	val providers: MutableMap<String, Provider> = HashMap<String, Provider>()
	var providerLimit = _maxProviderNr
	val uIdGenerator = _uIdGenerator
	var invocationStrategy: AbstractInvocationStrategy = RandomInvocationStrategy()

	fun get(): String? {
		increaseRequest()
		var response = invocationStrategy.chooseBetween(providers)?.get()
		decreaseRequest()
		return response;
	}

	@Synchronized
	fun runningRequest(): Int {
		return requests.toInt()
	}

	@Synchronized
	fun increaseRequest() {
		requests = AtomicInteger(requests.addAndGet(1))
	}

	@Synchronized
	fun decreaseRequest() {
		requests = AtomicInteger(requests.decrementAndGet())
	}

	fun size(): Int {
		return this.providers.size
	}

	fun reset() {
		this.providers.clear()
	}

	fun add(provider: Provider): BalancerResponseEnum {
		if (size() == providerLimit) return BalancerResponseEnum.TOO_MANY_PROVIDER
		this.providers.put(provider.get(), provider)
		return BalancerResponseEnum.OK
	}

	fun remove(provider: Provider): BalancerResponseEnum {
		if (!this.providers.containsValue(provider)) return BalancerResponseEnum.PROVIDER_DOESNT_EXISTS
		this.providers.remove(provider.get())
		return BalancerResponseEnum.OK
	}

	fun setStrategy(currentStrategy: AbstractInvocationStrategy) {
		this.invocationStrategy = currentStrategy;
	}

	fun increasetMaxNrTo(maxNr: Int) {
		this.providerLimit = maxNr
	}

	fun check() {
		this.providers.toMutableMap().forEach { (k, v) -> v.check() }
	}

	@Synchronized fun aliveProviders(): Int {
		return invocationStrategy.aliveProviders(this.providers);
	}
}


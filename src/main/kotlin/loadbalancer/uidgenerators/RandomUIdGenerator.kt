import java.util.UUID

class RandomUIdGenerator : AbstractUIdGenerator() {
	
	override fun generate(): String {
		return UUID.randomUUID().toString();
	}
}

class StaticUIdGenerator(_value: String) : AbstractUIdGenerator() {
	
	val value = _value
	
	override fun generate(): String {
		return value;
	}
}
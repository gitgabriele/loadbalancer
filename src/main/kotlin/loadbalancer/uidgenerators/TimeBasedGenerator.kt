class TimeBasedUIdGenerator(_duration : Long) : AbstractUIdGenerator() {
	
	val duration = _duration
	
	override fun generate(): String {
		Thread.sleep(duration * 1000)
		return "ID"
	}
}
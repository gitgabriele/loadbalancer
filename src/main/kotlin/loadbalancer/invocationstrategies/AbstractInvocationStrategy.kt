abstract class AbstractInvocationStrategy {
	fun chooseBetween(providers: Map<String, Provider>): Provider? {
		return chooseBetweenImpl(filterProviders(providers))
	}
	
	fun filterProviders(providers : Map<String, Provider>) : Map<String, Provider> {
		return providers.filter { (k, v) -> v.isAlive() }
	}
	
	fun aliveProviders(providers : Map<String, Provider>) : Int {
		return filterProviders(providers).keys.size
	}
	
	abstract	fun chooseBetweenImpl(providers: Map<String, Provider>): Provider?
	
	fun getByIndex(providers: Map<String, Provider>, index: Int): Provider? {
		return providers.get(providers.keys.elementAt(index))
	}
}
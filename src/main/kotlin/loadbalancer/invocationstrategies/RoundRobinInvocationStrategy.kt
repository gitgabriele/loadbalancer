class RoundRobinInvocationStrategy : AbstractInvocationStrategy() {
	
	var currentIndex = 0;
	
	override fun chooseBetweenImpl(providers: Map<String, Provider>): Provider? {
		if(currentIndex >= providers.size) {
			reset();
		}
		var result = getByIndex(providers, currentIndex)
		currentIndex = currentIndex + 1
		return result
	}
	
	fun reset(){
		currentIndex = 0;
	}
}
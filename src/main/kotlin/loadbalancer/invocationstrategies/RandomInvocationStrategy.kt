import kotlin.random.Random

class RandomInvocationStrategy : AbstractInvocationStrategy() {

	override fun chooseBetweenImpl(providers: Map<String, Provider>): Provider? {
		var maxValue = providers.size - 1
		if(maxValue.equals(0)) return getByIndex(providers, 0)
		return getByIndex(providers, Random.nextInt(0, maxValue));
	}
}
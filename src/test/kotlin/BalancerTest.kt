import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Test

class BalancerTest {

	val balancer = LoadBalancer(RandomUIdGenerator(), 2);

	@BeforeEach
	fun setup() {
		balancer.reset();
	}

	@Test
	@DisplayName("No providers are registered")
	fun noProvidersAreRegistered() {
		assertThat(balancer.size()).isEqualTo(0)
	}

	@Test
	@DisplayName("One provider is registered")
	fun oneProviderIsRegistered() {
		assertThat(balancer.add(Provider())).isEqualTo(BalancerResponseEnum.OK)
		assertThat(balancer.size()).isEqualTo(1)
	}

	@Test
	@DisplayName("More provider are registered")
	fun moreProviderAreRegistered() {
		assertThat(balancer.add(Provider())).isEqualTo(BalancerResponseEnum.OK)
		assertThat(balancer.add(Provider())).isEqualTo(BalancerResponseEnum.OK)
		assertThat(balancer.size()).isEqualTo(2)
	}

	@Test
	@DisplayName("Too many provider are registered")
	fun tooManyProviderAreRegistered() {
		assertThat(balancer.add(Provider())).isEqualTo(BalancerResponseEnum.OK)
		assertThat(balancer.add(Provider())).isEqualTo(BalancerResponseEnum.OK)
		assertThat(balancer.add(Provider())).isEqualTo(BalancerResponseEnum.TOO_MANY_PROVIDER)
		assertThat(balancer.size()).isEqualTo(2)
	}

	@Test
	@DisplayName("Random Access Strategy")
	fun randomAccessStrategy() {
		balancer.increasetMaxNrTo(5);
		assertThat(balancer.add(Provider(StaticUIdGenerator("1")))).isEqualTo(BalancerResponseEnum.OK)
		assertThat(balancer.add(Provider(StaticUIdGenerator("2")))).isEqualTo(BalancerResponseEnum.OK)
		assertThat(balancer.add(Provider(StaticUIdGenerator("3")))).isEqualTo(BalancerResponseEnum.OK)
		assertThat(balancer.add(Provider(StaticUIdGenerator("4")))).isEqualTo(BalancerResponseEnum.OK)
		assertThat(balancer.add(Provider(StaticUIdGenerator("5")))).isEqualTo(BalancerResponseEnum.OK)
		balancer.setStrategy(RandomInvocationStrategy());
		assertThat(
			balancer.get().equals("1") && balancer.get().equals("2") && balancer.get().equals("3") && balancer.get()
				.equals("4") && balancer.get().equals("5")
		).isEqualTo(false)
	}

	@Test
	@DisplayName("Round Robin Strategy")
	fun roundRobinStrategy() {
		balancer.increasetMaxNrTo(3);
		assertThat(balancer.add(Provider(StaticUIdGenerator("1")))).isEqualTo(BalancerResponseEnum.OK)
		assertThat(balancer.add(Provider(StaticUIdGenerator("2")))).isEqualTo(BalancerResponseEnum.OK)
		assertThat(balancer.add(Provider(StaticUIdGenerator("3")))).isEqualTo(BalancerResponseEnum.OK)
		var strategy = RoundRobinInvocationStrategy()
		balancer.setStrategy(strategy);
		assertThat(balancer.get()).isEqualTo("1");
		assertThat(balancer.get()).isEqualTo("2");
		assertThat(balancer.get()).isEqualTo("3");
		assertThat(balancer.get()).isEqualTo("1");
		assertThat(balancer.get()).isEqualTo("2");
		strategy.reset();
		assertThat(balancer.get()).isEqualTo("1");
	}

	@Test
	@DisplayName("manage specific provider")
	fun manageSpecificProvider() {
		var currentProvider = Provider()
		var otherProvider = Provider()
		assertThat(balancer.size()).isEqualTo(0)
		assertThat(balancer.add(currentProvider)).isEqualTo(BalancerResponseEnum.OK)
		assertThat(balancer.remove(otherProvider)).isEqualTo(BalancerResponseEnum.PROVIDER_DOESNT_EXISTS)
		assertThat(balancer.remove(currentProvider)).isEqualTo(BalancerResponseEnum.OK)
		assertThat(balancer.size()).isEqualTo(0)
	}

	@Test
	@DisplayName("check heart beat")
	fun checkHeartBeat() {
		var aliveStrategy: TestAliveStrategy = TestAliveStrategy()

		var provider1 = Provider(aliveStrategy);
		var provider2 = Provider(aliveStrategy);
		balancer.add(provider1)
		balancer.add(provider2)
		var checker = Checker(balancer, 10)
		checker.run()
		Thread.sleep(500)
		checker.stop()
		assertThat(aliveStrategy.callCount()).isEqualTo(2)
	}

	@Test
	@DisplayName("check heart beat interval")
	fun checkHeartBeatInterval() {
		var aliveStrategy: TimerAliveStrategy = TimerAliveStrategy()
		var interval = 3L;
		var provider = Provider(aliveStrategy);
		balancer.add(provider)
		var checker = Checker(balancer, interval)
		checker.run();
		Thread.sleep(interval * 1000 + 100)
		assertThat(aliveStrategy.interval()).isGreaterThanOrEqualTo(interval * 1000)
		checker.stop();
	}

	@Test
	@DisplayName("check exclude Providere if not Alive")
	fun checkExclueIfNotAlive() {
		var aliveStrategy: IsAliveStrategy = IsAliveStrategy()
		var notAliveStrategy: IsNotAliveStrategy = IsNotAliveStrategy()
		var interval = 3L;
		var provider1 = Provider(StaticUIdGenerator("1"), notAliveStrategy);
		var provider2 = Provider(StaticUIdGenerator("2"), aliveStrategy);
		testAliveProviders(provider1, provider2)
	}

	@Test
	@DisplayName("check exclude Provider is Reincluded")
	fun excludedProviderIsReincluded() {
		var aliveStrategy: IsAliveStrategy = IsAliveStrategy()
		var notAliveStrategy: IsNotAliveStrategy = IsNotAliveStrategy()
		var interval = 3L;
		var provider1 = Provider(StaticUIdGenerator("1"), notAliveStrategy);
		var provider2 = Provider(StaticUIdGenerator("2"), aliveStrategy);
		testAliveProviders(provider1, provider2)
		provider1.aliveStrategy(aliveStrategy)
		balancer.check();
		assertThat(balancer.aliveProviders()).isEqualTo(1)
		balancer.check();
		assertThat(balancer.aliveProviders()).isEqualTo(2)
	}

	fun testAliveProviders(provider1: Provider, provider2: Provider) {
		balancer.setStrategy(RoundRobinInvocationStrategy())
		balancer.add(provider1)
		balancer.add(provider2)
		assertThat(balancer.aliveProviders()).isEqualTo(2)
		balancer.check()
		assertThat(balancer.aliveProviders()).isEqualTo(1)
		assertThat(balancer.get()).isEqualTo("2")
	}
}
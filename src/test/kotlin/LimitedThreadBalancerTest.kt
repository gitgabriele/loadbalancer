import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Test

class LimitedThreadBalancerTest {
	
	val balancer = LoadBalancer(RandomUIdGenerator(), 2);

	@BeforeEach
	fun setup() {
		balancer.reset();
	}

	@Test
	@DisplayName("Throw an Exception If Max Nr Of Parallel Request Is Reached")
	fun throwExceptionIfMaxNrOfParallelRequestIsReached() {
		var aliveStrategy = IsAliveStrategy();
		balancer.add(Provider(TimeBasedUIdGenerator(15), aliveStrategy))
		var limitedBalancer = LimitedThreadBalancer(balancer, 2)
		Thread(
			Runnable() {
				limitedBalancer.get();
			}
		).start()
		Thread(
			Runnable() {
				limitedBalancer.get();
			}
		).start()
		var ex: Exception? = null
		var thread = Thread(
			Runnable() {
				try {
					limitedBalancer.get();
				} catch (e: Exception) {
					ex = e
				}
			}
		)
		thread.start()
		thread.join()
		assertThat(ex?.getLocalizedMessage()).isEqualTo("Too many parallel requests")
	}
}
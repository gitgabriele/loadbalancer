import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Test
 
class ProviderTest {
 
    @Test
    @DisplayName("Create Provider with his own id")
    fun get_retrieveId() {
    	var staticProvider = Provider(StaticUIdGenerator("1"))
        assertThat(staticProvider.get()).isEqualTo("1")
    }
	
	@Test
    @DisplayName("Create Provider with his own UID")
    fun get_retrieveUId() {
    	var staticProvider = Provider(RandomUIdGenerator())
        assertThat(staticProvider.get().length).isEqualTo(36)
    }
}
Some notes for the evaluator:
-) the software does not have a main but the requirements can be assessed by running one of the developed tests (see /src/test/kotlin)
-) the software is the result of TDD development and I tried to read and develop one scenario at a time incrementally without anticipating future requirements
-) 